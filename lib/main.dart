import 'package:flutter/material.dart';

import 'package:bonus/screens/bonus/bonusScreen.dart';
import 'package:bonus/screens/login/loginScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Бонусы',
      home: new LoginScreen(),
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/login':
            return new BonusCustomRoute(
                builder: (_) => new LoginScreen(), settings: settings);
          case '/home':
            return new BonusCustomRoute(
                builder: (_) => new BonusScreen(), settings: settings);
        }
      },
    );
  }
}

class BonusCustomRoute<T> extends MaterialPageRoute<T> {
  BonusCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    return new FadeTransition(opacity: animation, child: child);
  }
}
