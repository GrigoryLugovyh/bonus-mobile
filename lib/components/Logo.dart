import 'package:flutter/material.dart';

class LogoComponent extends StatelessWidget {
  final DecorationImage image;
  final double margin;
  final double size;

  LogoComponent({this.image, this.margin = 50.0, this.size = 200.0});

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(top: margin),
      width: size,
      height: size,
      alignment: Alignment.center,
      decoration: new BoxDecoration(image: image),
    );
  }
}
