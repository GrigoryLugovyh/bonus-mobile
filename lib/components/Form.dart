import 'package:flutter/material.dart';
import './Input.dart';

class FormComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.symmetric(horizontal: 20.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          new InputComponent(
            hint: "Имя пользователя",
            obscure: false,
            icon: Icons.person_outline,
          ),
          new InputComponent(
            hint: "Пароль",
            obscure: true,
            icon: Icons.lock_outline,
          )
        ],
      ),
    );
  }
}
