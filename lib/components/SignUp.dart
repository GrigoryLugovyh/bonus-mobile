import 'package:flutter/material.dart';

class SignUpComponent extends StatelessWidget {
  SignUpComponent();
  @override
  Widget build(BuildContext context) {
    return (new FlatButton(
      padding: const EdgeInsets.only(
        top: 160.0,
      ),
      onPressed: null, // можно сделать переход в поддержку
      child: new Text(
        "Не можете войти? Обратитесь в поддержку!",
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        softWrap: true,
        style: new TextStyle(
            fontWeight: FontWeight.w300,
            letterSpacing: 0.5,
            color: Colors.white,
            fontSize: 12.0),
      ),
    ));
  }
}
