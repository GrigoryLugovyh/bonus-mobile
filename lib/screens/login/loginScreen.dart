import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

import 'loginStyles.dart';
import 'loginAnim.dart';

import '../../components/Logo.dart';
import '../../components/Form.dart';
import '../../components/SignIn.dart';
import '../../components/SignUp.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen>
    with TickerProviderStateMixin {
  var animationStatus = 0;
  AnimationController loginButtonAnimationController;

  @override
  void initState() {
    super.initState();
    loginButtonAnimationController = new AnimationController(
        duration: new Duration(milliseconds: 3000), vsync: this);
  }

  @override
  void dispose() {
    loginButtonAnimationController.dispose();
    super.dispose();
  }

  Future<Null> playAnimation() async {
    try {
      await loginButtonAnimationController.forward();
      await loginButtonAnimationController.reverse();
    } on TickerCanceled {}
  }

  Future<bool> onWillPop() {
    return showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text('Хотите выйти?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Нет'),
              ),
              new FlatButton(
                onPressed: () => {
                      // выход из приложения
                    },
                child: new Text('Да'),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 0.4;
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        body: Container(
          decoration: new BoxDecoration(image: backgroundImage),
          child: Container(
            decoration: new BoxDecoration(
                gradient: new LinearGradient(
              colors: <Color>[
                const Color.fromRGBO(162, 146, 199, 0.8),
                const Color.fromRGBO(51, 51, 63, 0.9),
              ],
              stops: [0.2, 1.0],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(0.0, 1.0),
            )),
            child: new ListView(
              padding: const EdgeInsets.all(0.0),
              children: <Widget>[
                new Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: <Widget>[
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new LogoComponent(image: logoImage),
                        new FormComponent(),
                        new SignUpComponent()
                      ],
                    ),
                    animationStatus == 0
                        ? new Padding(
                            padding: const EdgeInsets.only(bottom: 50.0),
                            child: new InkWell(
                                onTap: () {
                                  setState(() {
                                    animationStatus = 1;
                                  });
                                  playAnimation();
                                },
                                child: new SignInComponent()),
                          )
                        : new LoginAnim(
                            buttonController:
                                loginButtonAnimationController.view,
                          ) // stagger animation
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
