import 'package:flutter/material.dart';

DecorationImage backgroundImage = getImage('background_image.jpg');

DecorationImage logoImage = getImage('logo_image.jpg');

DecorationImage getImage(String imageName) {
  return new DecorationImage(
      image: new ExactAssetImage('assets/$imageName'), fit: BoxFit.cover);
}
